<?php
use \******\products\locationManager;
use \******\utils\Pager;
use \******\catalog\Filter;
use ******\catalog\item\ItemSort;

class Catalog_Widget extends AbstractView {

	use \******\catalog\menu\TMenuComponent,
		\******\catalog\menu\tree_sub\TTreeMenuSubComponent,
		\******\catalog\brand\TBrandComponent,
		\******\catalog\TCatalogComponent;

	protected static $currentCatalog = [];

	protected $availableBlocks = ['hits','recommended','new'];

	public function getCurrentCatalog() {

		if(empty(self::$currentCatalog)) {

			$this->prepareWidgetUrl();

			self::$currentCatalog['menu_path'] = $this->menu_path;
			self::$currentCatalog['tree_path'] = $this->tree_path;
			self::$currentCatalog['tree_path_like'] = false;

			self::$currentCatalog['menu'] = null;


			if ($this->menu_path) {
				try {

					$menuObject = $this->menuComponent()->getByPath($this->menu_path);
					$menuObject->setHeader(strip_tags($menuObject->getHeader()));
					self::$currentCatalog['menu'] = $menuObject;

				} catch (Exception $e) {
					self::$currentCatalog['menu'] = null;
				}
			}

			if ($this->tree_path) {
				$like = false;
				if ($this->menu_path == 'all') {
					$this->tree_path = str_replace('all', '', $this->tree_path);
					$like = true;
				}
				try {
					$treeMenuSubObject = $this->treeMenuSubComponent()->getByPath($this->tree_path, $like);
					self::$currentCatalog['tree_menu'] = $treeMenuSubObject;
					self::$currentCatalog['tree_path_like'] = $like;
				} catch (Exception $e) {
					self::$currentCatalog['tree_menu'] = null;
				}
			} else {
				self::$currentCatalog['tree_menu'] = null;
			}
		}

		return self::$currentCatalog;
	}

	public function saveRecommendedBlock()
	{
		$requestCondition = isset($_REQUEST['currentBlock']) && in_array($_REQUEST['currentBlock'], $this->availableBlocks);
		$block = $requestCondition ? $_REQUEST['currentBlock'] : $this->availableBlocks[0];

		$user = User::getInstance();
		$user['recommendedBlock'] = $block;

		die(200);
	}

	public function recommendedList() {

		$urlData = explode('?', $_SERVER['REQUEST_URI']);
		$cacheUrl = isset($urlData[0])?$urlData[0]:null;


		if($cacheUrl != "/") {
			$cacheUrl = trim($cacheUrl, '/');
		}

		if($cacheUrl == 'orders') {
			return;
		}

		$cacheKey = "right_widget_" . $cacheUrl;
		$html = Yii::app()->cache->get($cacheKey);


		if($html === false) {
			$this->context = new System_Template_Context($this);
			$currentCatalog = $this->getCurrentCatalog();


			$filter = new Filter();
			$sort = new ItemSort();

			$menuObject = $currentCatalog['menu'];

			if ($menuObject) {
				$filter->setMenuBelong($menuObject->getId());
				$filter->setIsToy($menuObject->getIsToyMenu());

				$treeMenuSubObject = $currentCatalog['tree_menu'];

				if (!is_null($treeMenuSubObject)) {
					$filter->setTreeMenuSub($treeMenuSubObject);
				}
			}

			// хиты продаж
			$sort->setField('sales_month_count');
			$sort->setDirection('desc');
			$filter->setSort($sort);

			$this->context['hits_list'] = $this->getRecommendedProductsViewByFilter($filter, 'hits');

			// рекомендации
			$filter2 = clone $filter;
			$sort->setField('view_all');
			$sort->setDirection('asc');
			$filter2->setSort($sort);
			$filter2->setIsRecommended(1);

			$this->context['recommended_list'] = $this->getRecommendedProductsViewByFilter($filter2, 'recommended');


			// новинки
			$filter3 = clone $filter;
			$sort->setField('sales_month_count');
			$sort->setDirection('desc');
			$filter3->setSort($sort);
			$filter3->setIsNew(1);

			$this->context['new_list'] = $this->getRecommendedProductsViewByFilter($filter3, 'new');

			$user = User::getInstance();
			if (isset($user['recommendedBlock'])) {
				$this->context['newBlockActive'] = $user['recommendedBlock'] == $this->availableBlocks[2] ? 'active' : '';
				$this->context['hitsBlockActive'] = $user['recommendedBlock'] == $this->availableBlocks[0] ? 'active' : '';
				$this->context['recommendedBlockActive'] = $user['recommendedBlock'] == $this->availableBlocks[1] ? 'active' : '';
			} else {
				$this->context['newBlockActive'] = '';
				$this->context['hitsBlockActive'] = 'active';
				$this->context['recommendedBlockActive'] = '';
			}

			if ($this->context['hitsBlockActive'] == '' && $this->context['recommendedBlockActive'] == '' && $this->context['newBlockActive'] == '') {
				$this->context['newBlockActive'] = 'active';
			}

			$html = Template::getInstance($this->widgetTpl)->render($this->context);
			Yii::app()->cache->set($cacheKey, $html, 60*60*12);
		}

		return $html;
	}

	protected function getRecommendedProductsViewByFilter($filter, $type) {
		$perPage = 10;
		$catalogComponent = $this->catalogComponent();

		$pager = new Pager(1, $perPage);
		$itemSets = $catalogComponent->getItemSetListByFilter($filter, false, $pager);

		$itemSets = $this->setPrettySales($itemSets);
		$itemSets = $this->sortItemSets($itemSets, $type);

		// Ослабляем фильтр
		if (count($itemSets) < $perPage) {
			$filter->setMenuBelong(null);
			$filter->setIsToy(null);

			$currentCatalog = $this->getCurrentCatalog();
			$treeMenuSub = $currentCatalog['tree_menu'];

			// Расширяем категории поиска до родителей
			if (!is_null($treeMenuSub)) {
				$parents = $this->treeMenuSubComponent()->getParents($treeMenuSub);
				//unset($parents[0]); //будет искать даже в корне
				$treeMenuSubIdsList = array_map(function($o) { return $o->getId(); }, $parents);
				$filter->setTreeMenuSub($treeMenuSubIdsList);
			}

			// Исключаем уже выбранные товары
			if (count($itemSets) > 0) {
				$notInIdsList = array_map(function($o) { return $o->getItem()->getId(); }, $itemSets);
				$filter->setNotInIds($notInIdsList);
			}

			$pager->setPerPage($perPage - count($itemSets));
			$itemSetsAdvanced = $catalogComponent->getItemSetListByFilter($filter, false, $pager);

			$itemSetsAdvanced = $this->setPrettySales($itemSetsAdvanced);
			$itemSetsAdvanced = $this->sortItemSets($itemSetsAdvanced, $type);

			$itemSets = array_merge($itemSets, $itemSetsAdvanced);
		}

		$items = [];
		foreach ($itemSets as $key => $product) {
			$item = $product->getItem();

			$sales = $item->getSalesMonthCount();
			$items[] = [
				'itemSet' => $product,
				'sales' => $sales,
				'salesText' => self::pluralForm($sales, ['заказ','заказа','заказов'])
			];
		}
		
		$context = new System_Template_Context();
		$context['products'] = $items;
		return Template::getInstance('/products/product_recommended_list.html')->render($context);
	}

	public static function pluralForm($n, $forms) {
		$n = abs($n) % 100;
		$o = $n % 10;
		if ($n > 10 && $n < 20) return $forms[2];
		if ($o > 1 && $o < 5) return $forms[1];
		if ($o == 1) return $forms[0];
		return $forms[2];
	}

	/**
	 * Сетим накрученные продажи для продуктов
	 * @param $itemSets
	 * @return mixed
	 */
	protected function setPrettySales($itemSets)
	{
		foreach ($itemSets as $product) {
			$product->getItem()->setSalesMonthCount(self::getPrettySalesByItem($product->getItem()));
		}
		return $itemSets;
	}

	/**
	 * Сортировка по типу рекомендации
	 * @param $itemSets
	 * @param string $type
	 * @return mixed
	 */
	protected function sortItemSets($itemSets, $type = 'hits')
	{
		if ($type == 'hits') {
			usort($itemSets, function($a, $b) {
				$aSales = $a->getItem()->getSalesMonthCount();
				$bSales = $b->getItem()->getSalesMonthCount();

				if ($aSales == $bSales) {
					return 0;
				}
				return $aSales > $bSales ? -1 : 1;
			});
		}

		if ($type == 'recommended') {
			usort($itemSets, function($a, $b) {
				$aSales = $a->getItem()->getViewAll();
				$bSales = $b->getItem()->getViewAll();

				if ($aSales == $bSales) {
					return 0;
				}
				return $aSales < $bSales ? -1 : 1;
			});
		}

		if ($type == 'new') {
			usort($itemSets, function($a, $b) {
				$aId = $a->getItem()->getId();
				$bId = $b->getItem()->getId();

				if ($aId == $bId) {
					return 0;
				}
				return $aId < $bId ? -1 : 1;
			});
		}

		return $itemSets;
	}

	/**
	 * Накручиваем продажи и кешируем для продукта
	 * @param $item
	 * @return int
	 */
	public static function getPrettySalesByItem($item) {
		$productSales = $item->getSalesMonthCount();

		$cacheKey = 'pretty_sales_' . $item->getId() . ':' . $productSales;
		$cacheSales = \Yii::app()->cache->get($cacheKey);

		if ($cacheSales) {
			return $cacheSales;
		}

		$sales = ($productSales > 0) ? $productSales * 10 + mt_rand (1, 10) : mt_rand (1, 10);

		// Кешируем красивые продажи, чтобы при обновлении страницы они не изменились ;)
		$cacheTime = 2 * 60 * 60;
		\Yii::app()->cache->set($cacheKey, $sales, $cacheTime);

		return $sales;
	}

	/*
	* Возвращает представление виджета меню каталога
	*/
	public function menu() {

		$this->context = new System_Template_Context($this);

		$menuComponent = $this->menuComponent();
		$menuList = $menuComponent->getMenuList();

		$this->context['menu'] = $menuList;
		$currentCatalog = $this->getCurrentCatalog();

		$menuObject = $currentCatalog['menu'];



		if (!is_null($menuObject)) {
			$this->context['menu_current'] = $menuObject;

			$currentNode = $currentCatalog['tree_menu'];

			if($currentNode && $currentNode->getIsLeaf()) {
				$this->context['current_menu'] = $currentNode;

				$treePath = $currentCatalog['tree_path'];

				$this->brand_path = $currentNode->getPathTail($treePath);

				$brandComponent = $this->brandComponent();

				try {
					$currentBrand = $brandComponent->getByPath($this->brand_path);
					$this->context['brand_current'] = $currentBrand;
				} catch (Exception $e) {
					$currentBrand = null;
				}

				$brandList = $brandComponent->getMenuList($menuObject, $currentNode);

				$this->context['brands'] = $brandList;
			}


			$menuSubList = $this->treeMenuSubComponent()->getMenuList($menuObject, $currentNode, true);
			$this->context['tree'] = $menuSubList;
		}

		$user = User::getInstance();

		if (isset($user['personal'])) {
			$this->context['personal'] = true;
		}

		return  Template::getInstance($this->widgetTpl)->render($this->context);
	}

	public function menuList() {

		$cache = Yii::app()->cache;
		$key = 'main-page-menu';

		$html = $cache->get($key);

		if(!$html) {
			$this->context = new System_Template_Context($this);

			$menuComponent = $this->menuComponent();
			$menuList = $menuComponent->getMenuList();

			$menuListById = array();
			foreach ($menuList as $m) {
				$menuListById[$m['id']] = $m;
			}

			$menuIds = array(64, 4, 1, 2, 32, 16, 128, 512, 256, 2048, 1024, 8);
			$menuSorted = array();
			foreach ($menuIds as $id) {
				if (isset($menuListById[$id])) {
					$menuSorted[] = $menuListById[$id];
				}
			}

			$this->context->iterate('menu', $menuSorted, array($this, 'menuListCIterator'));
			$html = Template::getInstance($this->widgetTpl)->render($this->context);
			$cache->set($key, $html, 60*60);
		}

		return $html;
	}

	public function menuListCIterator($context, $varName, $var){
		$menuObject = $var;
		if (!is_null($menuObject)) {
			$treeMenuSubComponent = $this->treeMenuSubComponent();
			$currentNode = $treeMenuSubComponent->getByPk($menuObject['treeSubId']);
			$menuSubList = $treeMenuSubComponent->getMenuList($menuObject, $currentNode, true);
			$this->context[$varName.'.tree'] = $menuSubList;
		}
	}


	protected function prepareWidgetUrl() {

		$this->menu_path = '';
		$this->menusub_path = '';

		$currentUrl = $this->getUrl();

		if(preg_match('/catalog\/([\w\-]+)(\/.*)?/', $currentUrl, $m)) {
			$this->menu_path = $m[1];
			$this->tree_path = isset($m[2])?trim($m[2], '/'):false;
		} elseif(preg_match('/products\/(.*)?/', $currentUrl)) {

			if(!is_null(Products_LocationManager::getMenuPath())) {
				$this->menu_path = Products_LocationManager::getMenuPath();
			}

			if(!is_null(Products_LocationManager::getOtherPath())) {
				$this->tree_path = Products_LocationManager::getOtherPath();
			}
		} else {
			$this->menu_path = null;
			$this->menusub_path = null;
		}
	}

	protected function getUrl() {
		return trim($this->makeUrl('', true), '/');
	}
}


